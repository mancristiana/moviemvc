﻿using System.Web;
using System.Web.Mvc;

namespace Mandatory1_MovieCatalog
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
