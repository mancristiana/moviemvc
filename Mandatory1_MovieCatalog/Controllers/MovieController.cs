﻿using Mandatory1_MovieCatalog.Models;
using Mandatory1_MovieCatalog.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mandatory1_MovieCatalog.Controllers
{
    public class MovieController : Controller
    {
        //quick - and dirty - D.I. later
        ApplicationDbContext db = new ApplicationDbContext();
        MovieRepository movieRepo = new MovieRepository();
        
        // GET: Movie
        public ActionResult Index()
        {
            return View(db.Movies.ToList());
        }

        // Display the view containing the create form
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        // Update the db with the user input from the create form
        
        [HttpPost]
        public ActionResult CreateConfirmed(MovieModel movie)
        {
            //if(ModelState.IsValid)
            //{
                movieRepo.Insert(movie);
                return RedirectToAction("Index");
            //}
           
           // return View();

        }

        // Show the Edit view with the field info from the selected movie
        [HttpGet]
        public ActionResult Edit(int id)
        {
            // Find the student from the id and send to view.
            return View(movieRepo.Find(id));
        }

        // Update db with the info provided by user in the edit view
        [HttpPost]
        public ActionResult EditConfirmed(MovieModel movie)
        {
            if(ModelState.IsValid)
            {
                movieRepo.Update(movie);
                return RedirectToAction("Index"); //go back to index-view after succesful edit
            }
            return View();
        }

        // Show view containing details for the entry that is going to be deleted
        [HttpGet]
        public ActionResult Delete(int id)
        {
            return View(movieRepo.Find(id));
        }

        [HttpPost]
        public ActionResult DeleteConfirmed(int id)
        {
            movieRepo.Delete(id);
            return RedirectToAction("Index");
        }

 
    }
}