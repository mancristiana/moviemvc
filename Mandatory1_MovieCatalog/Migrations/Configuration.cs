namespace Mandatory1_MovieCatalog.Migrations
{
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Mandatory1_MovieCatalog.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "Mandatory1_MovieCatalog.Models.ApplicationDbContext";
        }

        protected override void Seed(Mandatory1_MovieCatalog.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

                 context.Movies.AddOrUpdate(
                  p => p.id,
                  new MovieModel[]
                  {
                        new MovieModel { ProductionYear = 2001, Title = "Amelie" },
                        new MovieModel { ProductionYear = 2016, Title = "Deadpool" },
                        new MovieModel { ProductionYear = 2009, Title = "Mr Nobody" }
                  }

               );
        }
    }
}
