﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mandatory1_MovieCatalog.Models
{
    public class MovieModel
    {
        public int id { get; set; }

        public string Title { get; set; }

        public int ProductionYear { get; set; }
    }
}