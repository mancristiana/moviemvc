﻿using Mandatory1_MovieCatalog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mandatory1_MovieCatalog.Repositories
{
    interface IMovieRepository
    {
        List<MovieModel> GetAll();
        MovieModel Find(int id);
        void Insert(MovieModel movie);
        void Update(MovieModel movie);
        void Delete(int id);
   
    }
}