﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Mandatory1_MovieCatalog.Models;

namespace Mandatory1_MovieCatalog.Repositories
{
    public class MovieRepository : IMovieRepository
    {
        ApplicationDbContext db = new ApplicationDbContext();

        public List<MovieModel> GetAll()
        {
            return db.Movies.ToList();
        }

        public MovieModel Find(int id)
        {
            return db.Movies.Find(id);
        }

        public void Insert(MovieModel movie)
        {
            db.Movies.Add(movie);
            db.SaveChanges();
        }

        public void Update(MovieModel movie)
        {
            db.Entry(movie).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            db.Movies.Remove(Find(id));
            db.SaveChanges();
        }

       
    }
}