﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Mandatory1_MovieCatalog.Startup))]
namespace Mandatory1_MovieCatalog
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
